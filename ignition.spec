# Generated by go2rpm 1.3
%if 0%{?fedora}
%bcond_without check
%else
# %%gocheck isn't currently provided on CentOS/RHEL
# https://bugzilla.redhat.com/show_bug.cgi?id=1982298
%bcond_with check
%endif

%global ignedgecommit b8d1b7a52c28fd5c33d15a0628d4b69f242f5c57
%global ignedgeshortcommit %(c=%{ignedgecommit}; echo ${c:0:7})

# For vendored gdisk
%global gdiskversion 1.0.10

# https://github.com/coreos/ignition
%global goipath         github.com/coreos/ignition
%global gomodulesmode   GO111MODULE=on
Version:                2.20.0

%gometa

%global golicenses      LICENSE
%global godocs          README.md docs/
%global dracutlibdir %{_prefix}/lib/dracut

Name:           ignition
Release:        %autorelease
Summary:        First boot installer and configuration tool

# Upstream license specification: Apache-2.0
# gdisk: GPL-2.0-or-later
License:        Apache-2.0 AND GPL-2.0-or-later
URL:            %{gourl}
Source0:        %{gosource}
Source1:        https://github.com/fedora-iot/ignition-edge/archive/%{ignedgecommit}/ignition-edge-%{ignedgeshortcommit}.tar.gz
# For vendored gdisk
Source2:        http://downloads.sourceforge.net/gptfdisk/gptfdisk-%{gdiskversion}.tar.gz

BuildRequires: libblkid-devel
BuildRequires: systemd-rpm-macros

# Requires for 'disks' stage
%if 0%{?fedora}
Recommends: btrfs-progs
%endif
Requires: dosfstools
Requires: dracut
Requires: dracut-network

%if 0%{?rhel} && 0%{?rhel} == 10
# For vendored gdisk
BuildRequires: gcc-c++
BuildRequires: libuuid-devel
BuildRequires: make
BuildRequires: ncurses-devel
BuildRequires: popt-devel
%endif

# Generated by `go-mods-to-bundled-provides.py | sort`
Provides: bundled(golang(cloud.google.com/go/compute/metadata)) = 0.2.3
Provides: bundled(golang(cloud.google.com/go/storage)) = 1.35.1
Provides: bundled(golang(cloud.google.com/go/storage/internal)) = 1.35.1
Provides: bundled(golang(cloud.google.com/go/storage/internal/apiv2)) = 1.35.1
Provides: bundled(golang(cloud.google.com/go/storage/internal/apiv2/storagepb)) = 1.35.1
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/arn)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/auth/bearer)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/awserr)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/awsutil)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/client)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/client/metadata)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/corehandlers)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/endpointcreds)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/processcreds)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/ssocreds)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/stscreds)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/csm)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/defaults)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/ec2metadata)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/endpoints)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/request)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/session)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/signer/v4)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/context)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/ini)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/s3shared)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/s3shared/arn)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/s3shared/s3err)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/sdkio)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/sdkmath)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/sdkrand)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/sdkuri)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/shareddefaults)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/strings)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/internal/sync/singleflight)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/checksum)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/eventstream)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/eventstream/eventstreamapi)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/json/jsonutil)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/jsonrpc)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/query)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/query/queryutil)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/rest)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/restjson)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/restxml)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/xml/xmlutil)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/s3)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/s3/s3iface)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/s3/s3manager)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/sso)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/ssooidc)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/sso/ssoiface)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/sts)) = 1.47.9
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/sts/stsiface)) = 1.47.9
Provides: bundled(golang(github.com/beevik/etree)) = 1.2.0
Provides: bundled(golang(github.com/containers/libhvee/pkg/kvp)) = 0.4.0
Provides: bundled(golang(github.com/coreos/go-semver/semver)) = 0.3.1
Provides: bundled(golang(github.com/coreos/go-systemd/v22/dbus)) = 22.5.0
Provides: bundled(golang(github.com/coreos/go-systemd/v22/journal)) = 22.5.0
Provides: bundled(golang(github.com/coreos/go-systemd/v22/unit)) = 22.5.0
Provides: bundled(golang(github.com/coreos/vcontext/json)) = 0.0.0-20230201181013.gitd72178a18687
Provides: bundled(golang(github.com/coreos/vcontext/path)) = 0.0.0-20230201181013.gitd72178a18687
Provides: bundled(golang(github.com/coreos/vcontext/report)) = 0.0.0-20230201181013.gitd72178a18687
Provides: bundled(golang(github.com/coreos/vcontext/tree)) = 0.0.0-20230201181013.gitd72178a18687
Provides: bundled(golang(github.com/coreos/vcontext/validate)) = 0.0.0-20230201181013.gitd72178a18687
Provides: bundled(golang(github.com/google/renameio/v2)) = 2.0.0
Provides: bundled(golang(github.com/google/uuid)) = 1.4.0
Provides: bundled(golang(github.com/mdlayher/vsock)) = 1.2.1
Provides: bundled(golang(github.com/mitchellh/copystructure)) = 1.2.0
Provides: bundled(golang(github.com/pin/tftp)) = 2.1.0
Provides: bundled(golang(github.com/pin/tftp/netascii)) = 2.1.0
Provides: bundled(golang(github.com/spf13/pflag)) = 1.0.6-0.20210604193023.gitd5e0c0615ace
Provides: bundled(golang(github.com/stretchr/testify/assert)) = 1.8.4
Provides: bundled(golang(github.com/vincent-petithory/dataurl)) = 1.0.0
Provides: bundled(golang(github.com/vmware/vmw-guestinfo/bdoor)) = 0.0.0-20220317130741.git510905f0efa3
Provides: bundled(golang(github.com/vmware/vmw-guestinfo/message)) = 0.0.0-20220317130741.git510905f0efa3
Provides: bundled(golang(github.com/vmware/vmw-guestinfo/rpcout)) = 0.0.0-20220317130741.git510905f0efa3
Provides: bundled(golang(github.com/vmware/vmw-guestinfo/rpcvmx)) = 0.0.0-20220317130741.git510905f0efa3
Provides: bundled(golang(github.com/vmware/vmw-guestinfo/vmcheck)) = 0.0.0-20220317130741.git510905f0efa3
Provides: bundled(golang(golang.org/x/net/bpf)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/context)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/http2)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/http2/hpack)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/http/httpguts)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/http/httpproxy)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/idna)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/internal/timeseries)) = 0.18.0
Provides: bundled(golang(golang.org/x/net/trace)) = 0.18.0
Provides: bundled(golang(golang.org/x/oauth2)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/authhandler)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/google)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/google/internal/externalaccount)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/google/internal/externalaccountauthorizeduser)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/google/internal/stsexchange)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/internal)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/jws)) = 0.14.0
Provides: bundled(golang(golang.org/x/oauth2/jwt)) = 0.14.0
Provides: bundled(golang(golang.org/x/sys/cpu)) = 0.14.0
Provides: bundled(golang(golang.org/x/sys/unix)) = 0.14.0
Provides: bundled(golang(google.golang.org/api/googleapi)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/googleapi/transport)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/iamcredentials/v1)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/internal)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/internal/cert)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/internal/gensupport)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/internal/impersonate)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/internal/third_party/uritemplates)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/iterator)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/option)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/option/internaloption)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/storage/v1)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/transport)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/transport/grpc)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/transport/http)) = 0.151.0
Provides: bundled(golang(google.golang.org/api/transport/http/internal/propagation)) = 0.151.0
Provides: bundled(golang(gopkg.in/yaml.v3)) = 3.0.1

%description
Ignition is a utility used to manipulate systems during the initramfs.
This includes partitioning disks, formatting partitions, writing files
(regular files, systemd units, etc.), and configuring users. On first
boot, Ignition reads its configuration from a source of truth (remote
URL, network metadata service, hypervisor bridge, etc.) and applies
the configuration.

############## validate subpackage ##############

%package validate

Summary:  Validation tool for Ignition configs
License:  Apache-2.0

%description validate
Ignition is a utility used to manipulate systems during the initramfs.
This includes partitioning disks, formatting partitions, writing files
(regular files, systemd units, etc.), and configuring users. On first
boot, Ignition reads its configuration from a source of truth (remote
URL, network metadata service, hypervisor bridge, etc.) and applies
the configuration.

This package contains a tool for validating Ignition configurations.

############## validate-redistributable subpackage ##############

%if 0%{?fedora}
%package validate-redistributable

Summary:   Statically linked validation tool for Ignition configs
License:   Apache-2.0
BuildArch: noarch

# In case someone has this subpackage installed, obsolete the old name
# Drop in Fedora 38
Obsoletes:     ignition-validate-nonlinux < 2.13.0-4

%description validate-redistributable
This package contains statically linked Linux, macOS, and Windows
ignition-validate binaries built through cross-compilation. Do not install it.
It is only used for building release binaries to be signed by Fedora release
engineering and uploaded to the Ignition GitHub releases page.
%endif

############## ignition-edge subpackage ##############

%package edge

Summary:  Enablement glue for Ignition on IoT/Edge systems
License:  Apache-2.0

%description edge
This package contains dracut modules, services and binaries needed to enable
Ignition on IoT/Edge systems.

############## ignition-grub subpackage ##############

%package ignition-grub

Summary:  Enablement glue for bootupd's grub2 config 
License:  Apache-2.0

%description ignition-grub
This package contains the grub2 config which is compatable with bootupd.

%prep
%if 0%{?fedora}
%goprep -k
%autopatch -p1
%else
%forgeautosetup -p1
%endif

tar xvf %{SOURCE1}

%if 0%{?rhel} && 0%{?rhel} == 10
# Prep vendored gdisk
tar xvf %{SOURCE2}
%endif

%build
export LDFLAGS="-X github.com/coreos/ignition/v2/internal/version.Raw=%{version} -X github.com/coreos/ignition/v2/internal/distro.selinuxRelabel=true "
%if 0%{?rhel} && 0%{?rhel} <= 8
# Disable writing ssh keys fragments on RHEL/CentOS <= 8
LDFLAGS+=' -X github.com/coreos/ignition/v2/internal/distro.writeAuthorizedKeysFragment=false '
%endif
%if 0%{?rhel}
# Need uncompressed debug symbols for debuginfo extraction
LDFLAGS+=' -compressdwarf=false '
%endif
export GOFLAGS="-mod=vendor"

echo "Building ignition..."
%gobuild -o ./ignition internal/main.go

echo "Building ignition-validate..."
%gobuild -o ./ignition-validate validate/main.go

%global gocrossbuild go build -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n')" -a -v -x

%if 0%{?fedora}
echo "Building statically-linked Linux ignition-validate..."
CGO_ENABLED=0 GOARCH=arm64 GOOS=linux %gocrossbuild -o ./ignition-validate-aarch64-unknown-linux-gnu-static validate/main.go
CGO_ENABLED=0 GOARCH=ppc64le GOOS=linux %gocrossbuild -o ./ignition-validate-ppc64le-unknown-linux-gnu-static validate/main.go
CGO_ENABLED=0 GOARCH=s390x GOOS=linux %gocrossbuild -o ./ignition-validate-s390x-unknown-linux-gnu-static validate/main.go
CGO_ENABLED=0 GOARCH=amd64 GOOS=linux %gocrossbuild -o ./ignition-validate-x86_64-unknown-linux-gnu-static validate/main.go

echo "Building macOS ignition-validate..."
GOARCH=amd64 GOOS=darwin %gocrossbuild -o ./ignition-validate-x86_64-apple-darwin validate/main.go
GOARCH=arm64 GOOS=darwin %gocrossbuild -o ./ignition-validate-aarch64-apple-darwin validate/main.go

echo "Building Windows ignition-validate..."
GOARCH=amd64 GOOS=windows %gocrossbuild -o ./ignition-validate-x86_64-pc-windows-gnu.exe validate/main.go
%endif

%if 0%{?rhel} && 0%{?rhel} == 10
# Build vendored gdisk
cd gptfdisk-%{gdiskversion}
make CXXFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" LDFLAGS="%{build_ldflags}"
%endif

%install
# dracut modules
install -d -p %{buildroot}/%{dracutlibdir}/modules.d
cp -r dracut/* %{buildroot}/%{dracutlibdir}/modules.d/
install -m 0644 -D -t %{buildroot}/%{_unitdir} systemd/ignition-delete-config.service
install -m 0755 -d %{buildroot}/%{_libexecdir}
ln -sf ../lib/dracut/modules.d/30ignition/ignition %{buildroot}/%{_libexecdir}/ignition-apply
ln -sf ../lib/dracut/modules.d/30ignition/ignition %{buildroot}/%{_libexecdir}/ignition-rmcfg

# ignition-grub
install -d -p %{buildroot}%{_libdir}/bootupd/grub2-static/configs.d
install -p -m 0644 grub2/ignition.cfg  %{buildroot}%{_libdir}/bootupd/grub2-static/configs.d/

# ignition
install -d -p %{buildroot}%{_bindir}
install -p -m 0755 ./ignition-validate %{buildroot}%{_bindir}

%if 0%{?fedora}
install -d -p %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-aarch64-apple-darwin %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-aarch64-unknown-linux-gnu-static %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-ppc64le-unknown-linux-gnu-static %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-s390x-unknown-linux-gnu-static %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-x86_64-apple-darwin %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-x86_64-pc-windows-gnu.exe %{buildroot}%{_datadir}/ignition
install -p -m 0644 ./ignition-validate-x86_64-unknown-linux-gnu-static %{buildroot}%{_datadir}/ignition
%endif

# The ignition binary is only for dracut, and is dangerous to run from
# the command line.  Install directly into the dracut module dir.
install -p -m 0755 ./ignition %{buildroot}/%{dracutlibdir}/modules.d/30ignition

%make_install -C ignition-edge-%{ignedgecommit}

%if 0%{?rhel} && 0%{?rhel} == 10
# Install vendored gdisk
cd gptfdisk-%{gdiskversion}
install -D -p -m 0755 sgdisk %{buildroot}%{_libexecdir}/ignition-sgdisk
install -D -p -m 644 COPYING %{buildroot}%{_datadir}/licenses/gdisk/COPYING
%endif

%if %{with check}
%check
# Exclude the blackbox tests
%gocheck -t tests
%endif

%files
%license %{golicenses}
%doc %{godocs}
%{dracutlibdir}/modules.d/30ignition/*
%{_unitdir}/ignition-delete-config.service
%{_libexecdir}/ignition-apply
%{_libexecdir}/ignition-rmcfg
# Vendored gdisk
%{_libexecdir}/ignition-sgdisk
%{_datadir}/licenses/gdisk/COPYING

%files validate
%doc README.md
%license %{golicenses}
%{_bindir}/ignition-validate

%if 0%{?fedora}
%files validate-redistributable
%license %{golicenses}
%dir %{_datadir}/ignition
%{_datadir}/ignition/ignition-validate-aarch64-apple-darwin
%{_datadir}/ignition/ignition-validate-aarch64-unknown-linux-gnu-static
%{_datadir}/ignition/ignition-validate-ppc64le-unknown-linux-gnu-static
%{_datadir}/ignition/ignition-validate-s390x-unknown-linux-gnu-static
%{_datadir}/ignition/ignition-validate-x86_64-apple-darwin
%{_datadir}/ignition/ignition-validate-x86_64-pc-windows-gnu.exe
%{_datadir}/ignition/ignition-validate-x86_64-unknown-linux-gnu-static
%endif

%files edge
%license %{golicenses}
%doc %{godocs}
%{dracutlibdir}/modules.d/35ignition-edge/*
%{dracutlibdir}/modules.d/10coreos-sysctl/*
%{dracutlibdir}/modules.d/99emergency-shell-setup/*
%{dracutlibdir}/modules.d/99journal-conf/*
%{_unitdir}/coreos-check-ssh-keys.service
%{_unitdir}/coreos-ignition-write-issues.service
%{_unitdir}/ignition-firstboot-complete.service
%{_libexecdir}/coreos-ignition-write-issues
%{_libexecdir}/coreos-check-ssh-keys

%files ignition-grub
%doc README.md
%license %{golicenses}
%{_libdir}/bootupd/grub2-static/configs.d/ignition.cfg

%changelog
%autochangelog
